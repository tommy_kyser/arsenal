<title>Kyser::Start</title>
</head>
<body>
<?php

$page = new \Kyser\content();
//$css->register_style('start_page','start_page.css');
?>

<style>
    .categories:nth-of-type(even) {
        background-color: #e1e2e1;
    }
</style>
<div class="container-fluid header">
    <div id="edit_btn" class="fixed-top">
        <a href="/start/">
            <i class="material-icons">
                save
            </i>
        </a>
    </div>
    <script>
        $(document).ready(function () {
            $('#edit_btn').click(function () {
                $('.edit_mode').toggleClass('active');
            });
        });
    </script>


    <div class="row">
        <div class="col-12 text-center">
            <h1>EDIT BOOKMARKS</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <form class="form-group edit_wrap" style="width:95%; margin: auto;">
                <div class="form-group edit_mode">
                    <input class="edit_mode form-control" id="add_cat" type="text" placeholder="Add Category">
                    <button type="button" class="btn btn-secondary edit_mode" id="cat_add">Add</button>
                </div>
            </form>
            <script>
                $(document).ready(function () {
                    $('#cat_add').click(function () {
                        var cat_name = $('#add_cat').val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                        $.post('?content=new_cat&cat=' + cat_name, function (data, status) {
                            location.reload();
                            $('#add_cat').val('');
                        });

                    });
                });
            </script>
        </div>
    </div>


    <?php
    $page->getData('start');

    $cats = $page->json['categories'];
    ?>


    <?php
    foreach ($cats

    as $key => $cat) {

    ?>
</div>
<div class="container-fluid categories">
    <div class="container-fluid">
        <div class="row cat text-center">
            <div class="col-12 text-center lists">
                <h2><?php echo $cat['name']; ?></h2>
                <form class="form-inline cat_edits">
                    <form class="form-group">
                        <div class="form-group edit_mode">
                            <input class="edit_mode form-control" id="edit_cat_<?php echo $key; ?>" type="text"
                                   placeholder="Rename">
                        </div>
                    </form>
                </form>
                <span class="form-group bmd-form-group">
                        <button type="button" class="btn btn-secondary edit_mode" id="<?php echo $key; ?>_edit">
                            Save
                        </button>
                    </span>
            </div>
            <form class="form-group del_cat_btn">
                <button type="button" class="btn btn-warning edit_mode" id="<?php echo $key; ?>_del">
                    Delete
                    Category
                </button>
            </form>
        </div>

    </div>
    <script>
        $('#<?php echo $key;?>_edit').click(function () {
            var new_cat_name = $('#edit_cat_<?php echo $key; ?>').val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
            $.post('?content=edit_cat&cat=<?php echo $key;?>&edit_cat_name=' + new_cat_name, function (data, status) {
                location.reload();
                $('#edit_cat_<?php echo $key; ?>').val('');
            });
        });
        $('#<?php echo $key;?>_del').click(function () {
            $.post('?content=del_cat&cat=<?php echo $key;?>', function (data, status) {
                location.reload();
            });
        });
    </script>

    <div class="row">
        <div class="col-12 text-center">
            <form class="form-group edit_wrap">
                <div class="form-group edit_mode">
                    <input class="edit_mode form-control" id="add_sub_<?php echo $key ?>" type="text"
                           placeholder="Add Sub Category">
                    <button type="button" class="btn btn-secondary edit_mode" id="sub_add_<?php echo $key ?>">Add
                    </button>
                </div>
            </form>
            <script>
                $(document).ready(function () {
                    $('#sub_add_<?php echo $key ?>').click(function () {

                        var sub_name = $('#add_sub_<?php echo $key ?>').val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                        $.post('?content=new_sub_cat&cat=<?php echo $key ?>&sub_name=' + sub_name, function (data, status) {
                            location.reload();
                            $('#add_sub_<?php echo $key ?>').val('');
                        });

                    });
                });
            </script>

        </div>
    </div>
    <div class="row">
        <?php
        foreach ($cat['sub_cats'] as $x => $sub_cat) {
            ?>
            <div class="col-3 text-center list">
                <div><h3><?php echo $sub_cat['name']; ?></h3>
                    <form class="form-group">
                        <div class="form-group edit_mode">
                            <button type="button" class="btn btn-secondary edit_mode"
                                    id="<?php echo $sub_cat['name']; ?>_del">
                                Remove Sub Category
                            </button>
                        </div>
                    </form>
                </div>
                <form class="form-group">
                    <div class="form-group edit_mode">
                        <input class="edit_mode form-control" id="<?php echo $sub_cat['name']; ?>_name" type="text"
                               placeholder="Enter Link Title">
                        <input class="edit_mode form-control" id="<?php echo $sub_cat['name']; ?>_url" type="text"
                               placeholder="Enter URL">
                        <button type="button" class="btn btn-secondary edit_mode"
                                id="<?php echo $sub_cat['name']; ?>_add">Add
                        </button>
                    </div>
                </form>
                <ul class="text-left links-list">
                    <?php
                    foreach ($sub_cat['links'] as $link => $val) {
                        ?>
                        <li>
                            <a href="<?php echo $val; ?>"><?php echo $link; ?></a>
                            <button type="button" class="btn btn-secondary edit_mode" id="<?php echo $link; ?>_del">
                                X
                            </button>
                            <script>
                                $(document).ready(function () {
                                    $('#<?php echo $link; ?>_del').click(function () {
                                        $.post('?content=del_entry&cat=backend&sub_name=<?php echo $sub_cat['name']; ?>&link_name=<?php echo $link; ?>', function (data, status) {
                                            location.reload();
                                            console.log('delete link');
                                        });

                                    });
                                });
                            </script>
                        </li>
                    <?php } ?>
                </ul>

            </div>
            <script>
                $(document).ready(function () {
                    $('#<?php echo $sub_cat['name']; ?>_add').click(function () {
                        var name = $('#<?php echo $sub_cat['name']; ?>_name').val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                        var link = $('#<?php echo $sub_cat['name']; ?>_url').val();
                        $.post('?content=new_entry&cat=<?php echo $key ?>&sub_name=<?php echo $sub_cat['name']; ?>&link_name=' + name + "&link_val=" + link, function (data, status) {
                            location.reload();
                            $('#<?php echo $sub_cat['name']; ?>_name').val('');
                            $('#<?php echo $sub_cat['name']; ?>_url').val('');
                        });

                    });
                    $('#<?php echo $sub_cat['name']; ?>_del').click(function () {
                        $.post('?content=del_sub_cat&cat=<?php echo $key ?>&sub_name=<?php echo $sub_cat['name']; ?>', function (data, status) {
                            location.reload();
                        });

                    });

                });

            </script>
        <?php } ?>
    </div>
</div>


    <?php } ?>

<?php




