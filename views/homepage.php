<title>Kyser::Start</title>
</head>
<body>
<?php

$page = new \Kyser\content();
//$css->register_style('start_page','start_page.css');
?>
<style>
    .edit_mode {
        display: none !important;
    }
</style>
<div class="container-fluid header">
    <div id="edit_btn" class="fixed-top">
        <a href="/start/edit">
            <i class="material-icons">
                build
            </i>
        </a>
    </div>
    <div class="row">
        <div class="col-12 text-center">
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <h1>BOOKMARKS</h1>
        </div>
    </div>
    <?php
    $page->getData('start');

    $cats = $page->json['categories'];
    ?>

    <?php
    foreach ($cats

    as $key => $cat) {

    ?>
</div>
    <div class="container-fluid categories">
        <div class="row cat text-center">
            <div class="col-12 text-center lists">
                <h2><?php echo $cat['name']; ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($cat['sub_cats'] as $x => $sub_cat) {
                ?>
                <div class="col-3 text-center list">
                    <div><h3><?php echo $sub_cat['name']; ?></h3>
                    </div>
                    <ul class="text-left links-list">
                        <?php
                        foreach ($sub_cat['links'] as $link => $val) {
                            ?>
                            <li>
                                <a href="<?php echo $val; ?>"><?php echo $link; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>

<?php } ?>

<?php




