<?php
require_once "resources.php";
//require_once "tasks.php";
require_once "header.php";
require_once "router.php";
require_once "content.php";
require_once "listeners.php";

$css = new resources();

route('/', function () {
    require __DIR__ . '/views/index.php';
});

route('/start', function () {
    require __DIR__ . '/views/homepage.php';
});
route('/start/edit', function () {
    require __DIR__ . '/views/homepage-edit.php';

});
$css->register_style('start_page','start_page.css');
$css->get_styles();




$action = $_SERVER['REQUEST_URI'];
dispatch($action);






require_once "footer.php";
