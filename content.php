<?php

namespace Kyser;
class content
{
    private $store = array();
    public $json = array();

    public function storeData($file)
    {
        $this->json = json_encode($this->store);
        file_put_contents(dirname(__FILE__) . '/data/' . $file . '.json', $this->json);
    }

    public function getData($file)
    {
        $data = file_get_contents(dirname(__FILE__) . '/data/' . $file . '.json');
        $this->json = json_decode($data, true);
    }

    public function register_content($target, $key, $data)
    {
        $this->json[$target][$key] = $data;

    }

    public function register_links($cat, $name, $link_name, $link_val)
    {
        $this->store = $this->json;
        $this->store['categories'][$cat]['sub_cats'][$name]['links'][$link_name] = $link_val;
        

    }

    public function remove_links($cat, $name, $link_name)
    {
        $this->store = $this->json;
        unset($this->store['categories'][$cat]['sub_cats'][$name]['links'][$link_name]);
        

    }

    public function add_sub_cat($cat, $name)
    {
        $this->store = $this->json;
        $this->store['categories'][$cat]['sub_cats'][$name]['links'] = [];
        $this->store['categories'][$cat]['sub_cats'][$name]['name'] = $name;
        

    }
    public function del_sub_cat($cat, $name){
        $this->store = $this->json;
        unset($this->store['categories'][$cat]['sub_cats'][$name]);
        

    }
    public function add_cat($cat)
    {
        $this->store = $this->json;
        $this->store['categories'][$cat]['sub_cats'] =[];
        $this->store['categories'][$cat]['name'] = $cat;
        

    }
    public function del_cat($cat){
        $this->store = $this->json;
        unset($this->store['categories'][$cat]);
        

    }
    public function edit_cat($cat, $new){
        $this->store = $this->json;
        $this->store['categories'][$cat]['name'] = $new;

    }

}





