<?php
class resources {
    private $styles = array();
    public function register_style($name, $uri){
        $this->styles[$name] = '<link rel="stylesheet" href="/css/'. $uri .'">';
    }
    public function get_styles(){
        foreach ($this->styles as $style){
            echo $style;
        }
    }
}