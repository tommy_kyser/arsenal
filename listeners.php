<?php
$update = new \Kyser\content();
// add new category
// ?new=category&val=category
$content = $_REQUEST['content'];
$val = $_REQUEST['val'];


$cat = $_REQUEST['cat'];
$sub_name = $_REQUEST['sub_name'];
$link_name = $_REQUEST['link_name'];
$link_val = $_REQUEST['link_val'];
$new = $_REQUEST['edit_cat_name'];
//?content=new_entry&cat=backend&sub_name=wordpress&link_name=bar&link_val=test.com
if ($content === 'new_entry') {
    $update->getData('start');
    $link = $link_val;
    $title = $link_name;


    $update->register_links($cat, $sub_name, $title, $link);

    $update->storeData('start');
}
if ($content === 'del_entry') {
    $update->getData('start');


    $update->remove_links($cat, $sub_name, $link_name);

    $update->storeData('start');
}

if ($content === 'new_sub_cat') {
    $update->getData('start');


    $update->add_sub_cat($cat, $sub_name);

    $update->storeData('start');
}
if ($content === 'del_sub_cat') {
    $update->getData('start');


    $update->del_sub_cat($cat, $sub_name);

    $update->storeData('start');
}
if ($content === 'new_cat') {
    $update->getData('start');


    $update->add_cat($cat);

    $update->storeData('start');
}
if ($content === 'del_cat') {
    $update->getData('start');


    $update->del_cat($cat);

    $update->storeData('start');
}
if ($content === 'edit_cat') {
    $update->getData('start');

    $update->edit_cat($cat, $new);

    $update->storeData('start');
}